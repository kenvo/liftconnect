<?php

use App\Mail\Mail;

require __DIR__ . '/vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

try {
    if (!isset($_POST['name']) || $_POST['name'] == null) {
        throw new \Exception("Please enter your name", 1);
    }
    if (!isset($_POST['email']) || $_POST['email'] == null) {
        throw new \Exception("please enter your email", 1);
    }
    if (!isset($_POST['phone']) || $_POST['phone'] == null) {
        throw new \Exception("Please enter your phone", 1);
    }
    if (!isset($_POST['content']) || $_POST['content'] == null) {
        throw new \Exception("Please enter your message", 1);
    }
    $msg = '';
    $msg .= "Your Name: {$_POST['name']} <br>";
    $msg .= "Phone Number: {$_POST['phone']} <br>";
    $msg .= "Email: {$_POST['email']} <br>";
    $msg .= "Content: {$_POST['content']} <br>";

    $mail = new Mail(getenv('SMTP_USER'), getenv('SMTP_PASSWORD'));
    $mail->send('LiftConnect', getenv('CONTACT_EMAIL'), 'LiftConnect Enquire Form', $msg);

    header_status(200);
    header('Content-Type: application/json');
    $response = array(
        'success' => true,
        'message' => 'Message has been sent',
    );
    echo json_encode($response);
} catch (\Exception $e) {
    header('Content-Type: application/json');
    header_status(500);
    $error = array(
        'error_code' => $e->getCode(),
        'error'      => $e->getMessage(),
    );
    echo json_encode($error);
}
