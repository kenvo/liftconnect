<?php

namespace App\Mail;

use PHPMailer;

class Mail
{
    public $smpt_user;
    public $smtp_password;
    public function __construct($smpt_user, $smtp_password)
    {
        $this->smpt_user     = $smpt_user;
        $this->smtp_password = $smtp_password;
    }
    public function send($from, $to, $subject, $body)
    {
        $mail = new PHPMailer;

        $mail->isSMTP();
        $mail->Host       = 'smtp.gmail.com';
        $mail->SMTPAuth   = true;
        $mail->Username   = $this->smpt_user;
        $mail->Password   = $this->smtp_password;
        $mail->SMTPSecure = 'tls';
        $mail->Port       = 587;

        $mail->setFrom($from, $subject);
        $mail->addAddress($to);

        $mail->isHTML(true);

        $mail->Subject = $subject;
        $mail->Body    = $body;

        if (!$mail->send()) {
            throw new \Exception("Can't send contact request", 1);
        } else {
            return true;
        }
    }
}
