$(function() {
    var mobile_menu = $('.menu-responsive');
    $('.close-menu, .lift-nav-bar').click(function() {
        mobile_menu.toggleClass('active');
    });
    $('.menu-body-item .preventDefault').click(function(event) {
        mobile_menu.removeClass('active');
        event.preventDefault();
        var scrollToId = $(this).attr('href');
        var scrollTop = $(scrollToId).offset().top - 60;
        $("html, body").animate({ scrollTop: scrollTop });
        return false;
    });

    // Add smooth scrolling to all links
    $(".page-scroll").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1000, function() {

                // Add hash (#) to URL when done scrolling (default click behavior)
                // window.location.hash = hash;
            });
        } // End if
    });

    // prallax js
    $('.parallax-window').parallax({imageSrc: 'assets/images/bottom-background.jpg'});
    $('.parallax-window-top').parallax({imageSrc: 'assets/images/top-background.jpg'});

    // Submit Form
    $("#sendMailForm").submit(function(event) {
        var nameElem = $('#sendMailName');
        var emailElem = $('#sendMailEmail');
        var phoneElem = $('#sendMailPhone');
        var contentElem = $('#sendMailContent');
        var name = nameElem.val();
        var email = emailElem.val();
        var phone = phoneElem.val();
        var content = contentElem.val();
        var url = 'sendmail.php';

        if (isNaN(phone)) {
            toastr.error('Phone must be number', { timeOut: 3000 });
            return false;
        }
        $('.loading').removeClass('hide');
        $.ajax({
            method: 'POST',
            url: url,
            data: {
                name: name,
                email: email,
                phone: phone,
                content: content
            },
            success: function(response) {
                console.log(response.message);
                toastr.success(response.message, { timeOut: 3000 });
                nameElem.val('');
                emailElem.val('');
                phoneElem.val('');
                contentElem.val('');
                $('.loading').addClass('hide');
            },
            error: function(err) {
                toastr.error(err.error, { timeOut: 3000 });
                $('.loading').addClass('hide');
            }
        });
        event.preventDefault();
    });
});

var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;

function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        videoId: 'NhqIxqMXzqI',
        events: {
            // 'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
    event.target.playVideo();
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING && !done) {
        // stopVideo();
        $('.play-video').hide();
    }
}

function stopVideo() {
    player.stopVideo();
}
$(document).on('click', '.play-video', function() {
    $(this).fadeOut('normal');
    player.playVideo();
});
